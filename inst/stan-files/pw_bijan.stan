data {
  int<lower=0> n;
  vector[n] doy;
  vector[n] gcc;
}
parameters {
  real<lower=0, upper=1> gStart;
  real<lower=0, upper=1> gMature;
  real<lower=0, upper=1> gSenescence;
  real<lower=0, upper=1> gEnd;
  real<lower=1, upper=365> dStart;
  real<lower=1, upper=365> dMature;
  real<lower=1, upper=365> dSenescence;
  real<lower=1, upper=365> dEnd;
  real<lower=1, upper=365> invVar;
}
transformed parameters {
  real sigma = 1 / sqrt(invVar);
  vector[n] mu;
  real eps = 1e-20;
  for (i in 1:n) {
    mu[i] = step(dStart - doy[i]) * (gStart) +
              step(doy[i] - dStart) * step(dMature - doy[i]) * ((gMature - gStart)/(dMature - dStart + eps)*(doy[i] - dStart) + gStart) +
              step(doy[i] - dMature) * step(dSenescence - doy[i]) * ((gMature - gSenescence)/(dMature - dSenescence + eps)*(doy[i] - dMature) + gMature) +
              step(doy[i] - dSenescence) * step(dEnd - doy[i]) * ((gEnd - gSenescence)/(dEnd - dSenescence + eps)*(doy[i] - dSenescence) + gSenescence) +
              step(doy[i] - dEnd) * (gEnd);
  }

}
model {

  gStart ~ uniform(0,1);
  gMature ~ uniform(gStart, 1);
  gSenescence ~ uniform(gStart, 1);
  gEnd ~ normal(gStart, 1/0.005);

  dStart ~ uniform(1, 365);
  dMature ~ uniform(dStart, 365);
  dSenescence ~ uniform(dMature, 365);
  dEnd ~ uniform(dSenescence, 365);

  invVar ~ gamma(0.01, 0.01);
  gcc ~ normal(mu, invVar);
}
