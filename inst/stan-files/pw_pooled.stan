data {
  int<lower=0> N;
  int<lower=0> P;
  vector[N] x;
  vector[N] y;
}
parameters {
  vector<lower=-1, upper=1>[P] g;
  ordered [P] t; 
  real<lower=0> sigma;
}
transformed parameters {
  vector[N] y_hat;

  for (i in 1:N) {
    if (x[i] < t[1]) {
      y_hat[i] = g[1];
    }else if (x[i] < t[2]) {
      y_hat[i] = (g[2] - g[1]) / (t[2] - t[1]);
    }else if (x[i] < t[3]) {
      y_hat[i] = (g[3] - g[2]) / (t[3] - t[2]);
    }else if (x[i] < t[4]) {
      y_hat[i] = (g[4] - g[3]) / (t[4] - t[3]);
    } else {
      y_hat[i] = g[4];
    }
  }
}
model {
  g ~ normal(0, 0.5);
  t ~ uniform(1, 365);
  sigma ~ normal(0,0.1);
  y ~ normal(y_hat, sigma);
}
