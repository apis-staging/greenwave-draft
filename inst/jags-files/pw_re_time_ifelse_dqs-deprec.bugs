# K: Number of years
# N: array with all readings
# M: number of "changepoints"
# k: index for year (1...K)
# t: N length vector (day of year)
# y: N length vector (observations)
# N_pred: ?
# t_pred: ?
# k_pred: ?


model {

  eps <- 1e-20  # avoids division-by-zero errors

  # -------- derived quantities
  for(i in 1:N_pred) {

    z_pred[i] <- k_pred[i] + 1  # TODO: supply as data or modify math accordingly...

    t_last_pred[i] <- lp_alpha[z_pred[i] - 1, M] - ifelse(leap_key[z_pred[i], 1], 366, 365)
    y_hat_mat_pred[i, 1] <- ifelse(t_pred[i] < lp_alpha[z_pred[i], 1], 1, 0) * (   
      lp_gamma[z_pred[i] - 1, M] +
        ((lp_gamma[z_pred[i], 1] - lp_gamma[z_pred[i] - 1, M]) / (lp_alpha[z_pred[i], 1] - t_last_pred[i] + eps)) *
        (t_pred[i] - t_last_pred[i])
    )

    for(m in 2:M) {
      y_hat_mat_pred[i, m] <-
        ifelse(t_pred[i] >= lp_alpha[z_pred[i], m - 1] && t_pred[i] < lp_alpha[z_pred[i], m], 1, 0) * (
          lp_gamma[z_pred[i], m - 1] +
            ((lp_gamma[z_pred[i], m] - lp_gamma[z_pred[i], m - 1]) / (lp_alpha[z_pred[i], m] - lp_alpha[z_pred[i], m - 1] + eps)) *
            (t_pred[i] - lp_alpha[z_pred[i], m - 1])
        )
    }

    t_next_pred[i] <- lp_alpha[z_pred[i], M] + 
        ((ifelse(leap_key[z_pred[i], 2], 366, 365) - lp_alpha[z_pred[i], M]) + lp_alpha[z_pred[i] + 1, 1])
    y_hat_mat_pred[i, M + 1] <- ifelse(t_pred[i] >= lp_alpha[z_pred[i], M], 1, 0) * (
      lp_gamma[z_pred[i], M] +
        ((lp_gamma[z_pred[i] + 1, 1] - lp_gamma[z_pred[i], M]) / (t_next_pred[i] - lp_alpha[z_pred[i], M])) *
        (t_pred[i] - lp_alpha[z_pred[i], M])
    )

    y_hat_pred[i] <- sum(y_hat_mat_pred[i, ])
    y_rep_pred[i] ~ dnorm(y_hat_pred[i], tau_y)

  }

  # -------- likelihood ----------------

  for(m in 1:M) {
    for (year in 1:(K+2)) {
      lp_alpha[year, m] <- alpha[m] + theta_alpha[year, m]
      lp_gamma[year, m] <- gamma[m] + theta_gamma[year, m]
    }
  }

  for(i in 1:N) {

    z[i] <- k[i] + 1  # TODO: supply as data or modify math accordingly...

    t_last[i] <- lp_alpha[z[i] - 1, M] - ifelse(leap_key[z[i], 1], 366, 365)
    y_hat_mat[i, 1] <- ifelse(t[i] < lp_alpha[z[i], 1], 1, 0) * (
      lp_gamma[z[i] - 1, M] +
      ((lp_gamma[z[i], 1] - lp_gamma[z[i] - 1, M]) / (lp_alpha[z[i], 1] - t_last[i] + eps)) *
      (t[i] - t_last[i])
    )

    for(m in 2:M) {
      y_hat_mat[i, m] <-
        ifelse(t[i] >= lp_alpha[z[i], m - 1] && t[i] < lp_alpha[z[i], m], 1, 0) * (
          lp_gamma[z[i], m - 1] +
          ((lp_gamma[z[i], m] - lp_gamma[z[i], m - 1]) / (lp_alpha[z[i], m] - lp_alpha[z[i], m - 1] + eps)) *
          (t[i] - lp_alpha[z[i], m - 1])
        )
    }
    
    t_next[i] <- lp_alpha[z[i], M] + 
      ((ifelse(leap_key[z[i], 2], 366, 365) - lp_alpha[z[i], M]) + lp_alpha[z[i] + 1, 1])
    y_hat_mat[i, M + 1] <- ifelse(t[i] >= lp_alpha[z[i], M], 1, 0) * (
      lp_gamma[z[i], M] +
      ((lp_gamma[z[i] + 1, 1] - lp_gamma[z[i], M]) / (t_next[i] - lp_alpha[z[i], M])) *
      (t[i] - lp_alpha[z[i], M])
    )

    y_hat[i] <- sum(y_hat_mat[i, ])
    y[i]   ~ dnorm(y_hat[i], tau_y)

    # Quantities used for model checking (e.g., simulated data for comparison
    # to the observed data and Bayesian P values).
    epsilon[i] <- y[i] - y_hat[i]
    y_rep[i] ~ dnorm(y_hat[i], tau_y)

  }

  p_sd <- step(sd(y_rep[]) - sd(y[]))
  p_mean <- step(mean(y_rep[]) - mean(y[]))

  # -------- priors ----------------

  # Priors for mean timing parameters.
  alpha[1] ~ dunif(1, 365)
  for(m in 2:M) {
    alpha[m] ~ dunif(alpha[m - 1], 365)
  }

  # Priors for mean greenness parameters.
  for(m in 1:M) {
    gamma[m] ~ dunif(0, 1)
  }

  # Priors for group-level effects in time.
  for(m in 1:M) {

    # Vector of variances for temporal random effects on timing parameters.
    varsigma_theta_alpha[m] ~ dunif(0, 25)
    tau_theta_alpha[m] <- 1 / varsigma_theta_alpha[m]^2

    # Vector of variances for temporal random effects on greenness parameters.
    varsigma_theta_gamma[m] ~ dunif(0, 1)
    tau_theta_gamma[m] <- 1 / varsigma_theta_gamma[m]^2

    for (year in 1:(K+2)) {
      theta_alpha[year, m] ~ dnorm(0, tau_theta_alpha[m])
      theta_gamma[year, m] ~ dnorm(0, tau_theta_gamma[m])
    }

  }

  # Observation error.
  sigma_y ~ dunif(0, 1)  #dgamma(0.01, 0.01)
  tau_y <- 1 / sigma_y^2

}
