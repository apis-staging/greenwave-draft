# K: Number of years, *including* "bookends" (the year prior and the year post)
# M: number of "changepoints"
# N: total number of observations
# P: the number of columns in the design matrix of year-varying covariates
# k: index for year corresponding to each year's position in X_k or theta
# t: N-length vector for time, within year, of observation (e.g., day of year)
# y: N-length vector of greenness values
# days_per_year: a K-length vector containing the number of days in each year k
# X_k: a [K, P] matrix of normalized time-varying covariates -- note that k == 1
#      and k == K correspond to our bookends. In cases in which K corresponds to
#      a year in the future, the X_k[, K] can be held at their means (n.e., 0), 
#      or values may be supplied based on the best-available information.
# Beta_alpha: an [M, P] matrix of coefficients for fixed effects on timings
# Beta_gamma: an [M, P] matrix of coefficients for fixed effects on greenness

# The matrices for Beta_* and theta_* and vectors varsigma_theta_* 
# (* = [alpha|gamma]) are supplied as data to permit flexible specification of
# effects (fixed, random, mixed, or none at all). By default, NA cells are 
# sampled by JAGS, while 0s are treated as constants. Of course, 0s also cause 
# the effect associated with a given term to evaluate to zero / fall out!

model {
  
  # -------- derived quantities ----------------
  
  for(m in 1:M) {
    alpha_intercept_out[m] <- ilogit(alpha_intercept[m]) * 
      (t_max - b1_out[m, 1]) + b1_out[m, 1]
    varsigma_theta_alpha_out[m] <- 
      ilogit(alpha_intercept[m] + varsigma_theta_alpha[m]) * 
        (t_max - b1_out[m, 1]) + b1_out[m, 1] - alpha_intercept_out[m]
    for(year in 1:K) {
      theta_alpha_out[year, m] <- 
        ilogit(alpha_intercept[m] + theta_alpha[year, m]) * 
          (t_max - b1_out[m, 1]) + b1_out[m, 1] - alpha_intercept_out[m]
      year_alpha_out[year, m] <- 
        alpha_intercept_out[m] + theta_alpha_out[year, m]
      year_gamma_out[year, m] <- gamma_intercept[m] + theta_gamma[year, m]
    }
  }
  
  for(p in 1:P) {
    Beta_alpha_out[1, p] <- (ilogit(alpha_intercept[1] + Beta_alpha[1, p]) * 
      (t_max - b1_out[1, p]) + b1_out[1, p] - alpha_intercept_out[1]) / X_sd[p]
    for(m in 2:M) {
      b1_out[m, p] <- alpha_intercept_out[m - 1]
      Beta_alpha_out[m, p] <- (ilogit(alpha_intercept[m] + Beta_alpha[m, p]) * 
        (t_max - b1_out[m, p]) + b1_out[m, p] - alpha_intercept_out[m]) / X_sd[p]
    }
  }
  
  # -------- likelihood ----------------
  
  # Overall mean for timing (alpha_intercept) and greenness (gamma_intercept) 
  # parameters at each changepoint (m) plus fixed and random (theta) year 
  # effects. Note that the "loop" over years includes bookends.
  
  for (year in 1:K) {
    for(m in 2:M) {
      b1[year, m] <- alpha[year, m - 1]
    }
  }
  
  for (year in 1:K) {
    for(m in 1:M) {
      x[year, m] <- alpha_intercept[m] + 
        X_k[year, ] %*% Beta_alpha[m, 1:P] + 
        theta_alpha[year, m]
      alpha[year, m] <- ilogit(x[year, m]) * 
        (days_per_year[year] - b1[year, m]) + b1[year, m]
      gamma[year, m] <- gamma_intercept[m] +
        X_k[year, ] %*% Beta_gamma[m, 1:P] + 
        theta_gamma[year, m]
    }
  }
  
  for(n in 1:N) {
    
    # Calculate means for the first segment in that year.
    t_last[n] <- alpha[k[n] - 1, M] - days_per_year[k[n] - 1]
    y_hat_mat[n, 1] <- ifelse(t[n] > 0 && t[n] <= alpha[k[n], 1], 1, 0) * (
      gamma[k[n] - 1, M] +
        ((gamma[k[n], 1] - gamma[k[n] - 1, M]) / 
          (alpha[k[n], 1] - t_last[n] + eps)) *
        (t[n] - t_last[n])
    )
    
    # Calculate the means for the internal segments in that year.
    for(m in 2:M) {
      y_hat_mat[n, m] <-
        ifelse(t[n] > alpha[k[n], m - 1] && t[n] <= alpha[k[n], m], 1, 0) * (
          gamma[k[n], m - 1] +
            ((gamma[k[n], m] - gamma[k[n], m - 1]) / 
              (alpha[k[n], m] - alpha[k[n], m - 1] + eps)) *
            (t[n] - alpha[k[n], m - 1])
        )
    }
    
    # Calculate the mean for the last segment in that year.
    y_hat_mat[n, M + 1] <- 
      ifelse(t[n] > alpha[k[n], M] && t[n] <= days_per_year[k[n]], 1, 0) * (
      gamma[k[n], M] +
        ((gamma[k[n] + 1, 1] - gamma[k[n], M]) / 
          (alpha[k[n] + 1, 1] + days_per_year[k[n]] - alpha[k[n], M])) * 
        (t[n] - alpha[k[n], M])
    )
    
    y_hat[n] <- sum(y_hat_mat[n, ])
    y[n] ~ dnorm(y_hat[n], tau_y)
    
    # Quantities used for model checking (e.g., simulated data for comparison
    # to the observed data and Bayesian P values).
    epsilon[n] <- y[n] - y_hat[n]
    y_rep[n] ~ dnorm(y_hat[n], tau_y)
    
  }
  
  p_sd <- step(sd(y_rep[]) - sd(y[]))
  p_mean <- step(mean(y_rep[]) - mean(y[]))
  
  # -------- priors ----------------
  
  # Priors for mean timing parameters and mean greenness parameters.
  for(m in 1:M) {
    alpha_intercept[m] ~ dnorm(mu_alpha_intercept[m], 1 / 0.5^2)
    gamma_intercept[m] ~ dunif(0, 1)
  }
  
  # Prior for Betas.
  for(m in 1:M) {
    for(p in 1:P) {
      Beta_alpha[m, p] ~ dnorm(0, 1 / 1.5^2)
      Beta_gamma[m, p] ~ dnorm(0, 1 / 100^2)
    }
  }
  
  # Priors for group-level effects in time.
  for(m in 1:M) {
    
    # Vector of variances for temporal random effects on timing parameters.
    varsigma_theta_alpha[m] ~ dunif(0, 1.5)  #dunif(0, 0.25)
    tau_theta_alpha[m] <- 1 / varsigma_theta_alpha[m]^2
    
    # Vector of variances for temporal random effects on greenness parameters.
    varsigma_theta_gamma[m] ~ dunif(0, 0.25)  # or (less- to more-informative): 0.125, 0.0625
    tau_theta_gamma[m] <- 1 / varsigma_theta_gamma[m]^2
    
    for (year in 1:K) {
      theta_alpha[year, m] ~ dnorm(0, tau_theta_alpha[m])
      theta_gamma[year, m] ~ dnorm(0, tau_theta_gamma[m])
    }
    
  }
  
  # Observation error.
  sigma_y ~ dunif(0, 1)
  tau_y <- 1 / sigma_y^2
  
  # Constants (recycled above).
  eps <- 1e-20  # avoids division-by-zero errors
  t_max <- 365  # the max day of year, as required by the derived quantities
  
}
