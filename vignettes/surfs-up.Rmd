---
title: "Intro to the greenwave package"
author:
  - Luke Zachmann
  - Jesse Anderson
date: "`r format(Sys.time(), '%b %d , %Y')`"
output:
  rmarkdown::html_vignette:
    fig_caption: yes
    includes:
      before_body: preamble-mathjax.tex
vignette: >
  %\VignetteIndexEntry{Intro to the greenwave package}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

# greenwave

First let's load the packages we need.
```{r libs, echo=TRUE, message=FALSE}
library(greenwave)
library(tibble)
library(ggplot2)
library(ggforce)
```

```{r, fig.show='hold'}
t <- c("t[0]" = 0, "t[1]" = 3, "t[2]" = 5)
g <- c("g[0]" = 1, "g[1]" = 1, "g[2]" = 4)


param_key <- gw_param_key(transitions = c('init', 'start', 'maturity'))
gw_join_param_vals(param_key, t, g)

my_radius <- 1.5
circ <- gw_circle_at_radius_r(timing = t, greenness = g, index = 1, radius = my_radius)
circ_tangents <- as_tibble(circ$tangents_coords, .name_repair = 'minimal')
circ_center <- as_tibble(circ$circ_center_coords, .name_repair = 'minimal')
circ_arc <- as_tibble(circ$arc_coords, .name_repair = 'minimal')

gw_vis_lin_fun(param_key, t, g) +
  geom_point(data = circ_tangents, aes(x = x_tangent, y = y_tangent),
             color = 'orange', size = 3) +
  geom_point(data = circ_center, aes(x = h, y = k),
             color = 'blue', size = 3) +
  geom_line(data = circ_arc, aes(x = p_x, y = p_y),
            color = 'red', size = 2) +
  coord_equal() #+
  geom_circle(data = circ_center, aes(x0 = h, y0 = k, r = my_radius),
              color = 'yellow', size = .5)
```
