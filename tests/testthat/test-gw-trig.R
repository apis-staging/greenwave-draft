context("test-gw-trig")

test_that("pi equals 180 degrees", {
  expect_equal(gw_rad_to_deg(pi), 180)
})

test_that("180 degrees equals pi", {
  expect_equal(gw_deg_to_rad(180), pi)
})
