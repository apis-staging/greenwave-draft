# News / Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Adding...
- First real bundle of utilities

## [0.1.1] - 2019-03-XX
### Fixed
- TBD

### Added
- TBD

## 0.1.0 - 2019-03-19
### Added
- Package scaffolding

[Unreleased]: https://gitlab.com/apis-staging/greenwave/compare/v0.1.1...HEAD
[0.1.1]: https://gitlab.com/apis-staging/greenwave/compare/v0.1.0...v0.1.1
